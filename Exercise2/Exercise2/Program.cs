﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            var colour = Console.ReadLine();

            switch (colour)
            {
                case "blue":
                    Console.WriteLine("You chose blue - The sky is blue");
                    break;
                case "red":
                    Console.WriteLine("You chose red - Apples can be red");
                    break;
                default:
                    Console.WriteLine("You chose the wrong colour");
                    break;
            }
        }
    }
}
